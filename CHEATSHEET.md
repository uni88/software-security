# Software security

## Compile source files

From the source directory, run `make`.

## GDB

### Common commands

- `gdb foo` to start the debugger for executable `foo`.

When inside GDB:

- `checksec`, check compiler protections that are in place.
- `pdisass foo`, check assembler equivalent of a function.
- `b *0xn`, set a breakpoint before instruction `0xn`.
- `r foo`, execute the program with input `foo`.
- `c`, resume program execution after stopping at a breakpoint.
- `s`, execute only one instruction after stopping at a breakpoint.
- `pattern arg n`, to set up a random string of length `n` as arguments for the
  program.
- `pattern arg n m`, to set up two random strings of length `n` and `m`
  respectively as arguments for the program. Also works with more than two
  arguments.
- `pattern create n foo`, to write a pattern of size `n` inside file `foo`.
- `delete breakpoints`, delete all breakpoints.
- `p foo`, print a summary - including the address - of function `foo`.
- `skeleton argv foo.py`, writes a script file into `foo.py` to attack current
  program.
- `shellcode search linux`, return a list of available Linux shellcodes, along
  with their size.
- `shellcode display n`, displays shellcode identified by `n`. The actual
  shellcode is inside the variable `code`. `752` is a working shellcode.

### Common pipelines

- `pattern arg n | r | pattern search`, with a big enough `n` returns which
  offset of the input ends up in which location of the stack, in case of CLI
  arguments.
- `pattern create n foo | r < foo | pattern search`, with a big enough `n`
  returns which offset of the input ends up in which location of the stack, in
  case of user input.

## Exploit using command line arguments

To exploit program `foo` using one or more command line arguments:

1. Write the injection vector into a file `bar`.
2. Run `./foo $(cat bar)`.

Also works with multiple arguments:

1. Write the injection vectors into files `bar` and `baz`.
2. Run `./foo $(cat bar) $(cat baz)`.

## Exploit using standard input

To exploit program `foo` using its standard input from the shell:

1. Write the injection vector into a file `bar`.
2. Run `cat bar - | ./foo`.

Also works with multiple inputs:

1. Write the injection vectors into files `bar` and `baz`, remembering to end
   the first injection vector with a newline character - `\n`.
2. Run `cat bar baz - | ./foo`.

To exploit program `foo` using its standard input from `gdb`:

1. Write the injection vector into a file `bar`.
2. Run `gdb foo`.
3. Run `r < bar`.

## Build injection vector with python

```python
struct.pack("I", return_address) # write addresses.
```

When building an injection vector for a **shellcode**, it's important to not put
the shellcode at the end of the vector - right before the return address. The
shellcode uses a small portion of the stack as a **workspace** to perform some
operation and, if not given some breathing room, will overwrite the return
address, causing a `SEGFAULT`. This can be avoided by putting padding or a
nopsled between the shellcode and the return address.

## Use nopsled

When unsure about the target address, start the injection vector with a
**nopsled**: a sequence of `nop` instructions. The `nop` instruction is `\x90`.

## Check content of injection file

To check the content of injection file `foo`, run `cat foo | xxd`.

## ROP

To list the gadget available when trying to attack `foo`.

```bash
ROPgadget.py --binary foo
```

## KLEE

See youtube tutorials from **AdaLogics**.

### Set up

Pull and run KLEE in a container using **Docker**.

```bash
docker pull klee/klee
docker run --rm -ti --ulimit='stack=1:-1' klee/klee
```

### Compile a sourcefile

To compile sourcefile `foo.c`.

```bash
clang --emit-llvm -c foo.c
```

The result of this compilation is a `foo.bc` file, which is an intermediate
representation file, upon which we will apply our analysis.

### To analyze a program

To analyze program `foo.bc`.

```bash
klee foo.bc
```

These will generate a number of tests, which can be then seen in the directory
specified in the script output, or in the `klee-last` directory, which always
store the test of the last performed analysis.

When _libc_ are included, we have to provide a model to mimic those. To analyze
program `bar.bc` which contains _libc_ library calls, using model `uclibc`.

```bash
klee --libc=uclibc bar.bc
```

To look at test case `baz.ktest`.

```bash
ktest-tool baz.ktest
```

### Common patterns

```c
// For a file to be testable, it has to include the klee library.
#include <klee/klee.h>

int main() {
  // To create a symbolic variable from a real variable `a`.
  int a;
  klee_make_symbolic(&a, sizeof(a), "a");

  // To create a symbolic variable from a real vector variable `re`.
  char re[SIZE];
  klee_make_symbolic(re, sizeof re, "re");

  // To use symbolic analysis for a real user input scenario. N.b. this uses
  // libc and will need a model for those to correctly detect bugs.
  char str[517];
  FILE *badfile;
  // We have to comment out the actual input operations.
  // badfile = fopen("badfile", "r");
  // fread(str, sizeof(char), 517, badfile);
  klee_make_symbolic(str, sizeof str, "str");
  // We have to append the end character, to avoid false positives.
  str[516] = '\0';

  return 0;
}
```
