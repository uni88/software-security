# Software security

1. Run the VM by double-clicking on your VM file.
2. Login as `andrew`, using password `andrew`.
3. Install **PEDA**, following
   [instructions](https://github.com/longld/peda#installation).
4. Clone this repository
   `git clone https://gitlab.com/uni88/software-security.git`.
5. Become root by running `su -`, using password `osboxes.org`.
6. Disable randomization by running `sh randomization_disable.sh`.
